﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Tsibiy_Lab_5
{
    public partial class WindowUpdateWorkers : Form
    {
        string ConnectionString = "Server=tcp:itacademyukraine.database.windows.net;Database=r.cibiy;User ID =r.cibiy;Password=Dbiyz117;Trusted_Connection=False;Encrypt=True;";
        public WindowUpdateWorkers()
        {
            InitializeComponent();
        }

        private void WindowUpdateWorkers_Load(object sender, EventArgs e)
        {
            LoadWorkersID();
            LoadJobTitleList();
        }



        private void LoadWorkersID()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlCommand comm = new SqlCommand("select * from workers ", con);
            SqlDataReader DR = comm.ExecuteReader();
            cmbWorkersID.Items.Clear();

            while (DR.Read())
            {
                Workers wk = new Workers();
                wk.WorkersID = (int)DR[0];
                cmbWorkersID.Items.Add(wk);
                cmbWorkersID.DisplayMember = "WorkersID";
            }
            con.Close();

        }


        private void LoadJobTitleList()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlCommand comm = new SqlCommand("SELECT * FROM Job_title", con);
            SqlDataReader DR = comm.ExecuteReader();
            cmbJobTitle.Items.Clear();

            while (DR.Read())
            {
                JobTitle jt = new JobTitle();
                jt.id = (int)DR[0];
                jt.name = DR[1].ToString();
                cmbJobTitle.Items.Add(jt);
                cmbJobTitle.DisplayMember = "name";
            }
            con.Close();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString);
                con.Open();
                Workers WR = (Workers)cmbWorkersID.SelectedItem;
                JobTitle JT = (JobTitle)cmbJobTitle.SelectedItem;
                SqlCommand comm = new SqlCommand("UPDATE [dbo].[Workers]SET [first_name] = '" + tbfirst_name.Text + "' ,[last_name] =  '" + tblast_name.Text + "' ,[job_title_id] = " + JT.id.ToString() + " ,[phone] = " + tbphone.Text + " ,[mail] = '" + tbmail.Text + "' ,[address] = '" + tbaddress.Text + "' ,[birthday] = '" + tbbirthday.Text + "' ,[start_job] = '" + tbstart_job.Text + "' WHERE workers_id = " + WR.WorkersID.ToString(), con);
                comm.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Дані Успішно змінено Для користувача " + WR.WorkersID.ToString());


            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message.ToString());
            }
        }






        private void cmbJobTitle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbWorkersID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

    }
}

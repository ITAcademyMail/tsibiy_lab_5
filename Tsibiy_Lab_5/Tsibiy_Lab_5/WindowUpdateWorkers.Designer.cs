﻿namespace Tsibiy_Lab_5
{
    partial class WindowUpdateWorkers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbJobTitle = new System.Windows.Forms.ComboBox();
            this.tbbirthday = new System.Windows.Forms.TextBox();
            this.tbstart_job = new System.Windows.Forms.TextBox();
            this.lblbirthday = new System.Windows.Forms.Label();
            this.lblstart_job = new System.Windows.Forms.Label();
            this.tbaddress = new System.Windows.Forms.TextBox();
            this.tbmail = new System.Windows.Forms.TextBox();
            this.tbphone = new System.Windows.Forms.TextBox();
            this.tblast_name = new System.Windows.Forms.TextBox();
            this.tbfirst_name = new System.Windows.Forms.TextBox();
            this.lbllast_name = new System.Windows.Forms.Label();
            this.lblphone = new System.Windows.Forms.Label();
            this.lbljob_title_id = new System.Windows.Forms.Label();
            this.lblmail = new System.Windows.Forms.Label();
            this.lbladdress = new System.Windows.Forms.Label();
            this.lblfirst_name = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbWorkersID = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(36, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 25);
            this.label1.TabIndex = 37;
            this.label1.Text = "Таблиця Працівників";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cmbJobTitle
            // 
            this.cmbJobTitle.FormattingEnabled = true;
            this.cmbJobTitle.Location = new System.Drawing.Point(128, 125);
            this.cmbJobTitle.Name = "cmbJobTitle";
            this.cmbJobTitle.Size = new System.Drawing.Size(128, 21);
            this.cmbJobTitle.TabIndex = 35;
            this.cmbJobTitle.SelectedIndexChanged += new System.EventHandler(this.cmbJobTitle_SelectedIndexChanged);
            // 
            // tbbirthday
            // 
            this.tbbirthday.Location = new System.Drawing.Point(128, 231);
            this.tbbirthday.Name = "tbbirthday";
            this.tbbirthday.Size = new System.Drawing.Size(128, 20);
            this.tbbirthday.TabIndex = 34;
            // 
            // tbstart_job
            // 
            this.tbstart_job.Location = new System.Drawing.Point(128, 257);
            this.tbstart_job.Name = "tbstart_job";
            this.tbstart_job.Size = new System.Drawing.Size(128, 20);
            this.tbstart_job.TabIndex = 33;
            // 
            // lblbirthday
            // 
            this.lblbirthday.AutoSize = true;
            this.lblbirthday.Location = new System.Drawing.Point(19, 234);
            this.lblbirthday.Name = "lblbirthday";
            this.lblbirthday.Size = new System.Drawing.Size(98, 13);
            this.lblbirthday.TabIndex = 32;
            this.lblbirthday.Text = "Дата народження";
            this.lblbirthday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblstart_job
            // 
            this.lblstart_job.AutoSize = true;
            this.lblstart_job.Location = new System.Drawing.Point(9, 260);
            this.lblstart_job.Name = "lblstart_job";
            this.lblstart_job.Size = new System.Drawing.Size(113, 13);
            this.lblstart_job.TabIndex = 31;
            this.lblstart_job.Text = "Розпочав працювати";
            this.lblstart_job.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbaddress
            // 
            this.tbaddress.Location = new System.Drawing.Point(128, 205);
            this.tbaddress.Name = "tbaddress";
            this.tbaddress.Size = new System.Drawing.Size(128, 20);
            this.tbaddress.TabIndex = 30;
            // 
            // tbmail
            // 
            this.tbmail.Location = new System.Drawing.Point(128, 179);
            this.tbmail.Name = "tbmail";
            this.tbmail.Size = new System.Drawing.Size(128, 20);
            this.tbmail.TabIndex = 29;
            // 
            // tbphone
            // 
            this.tbphone.Location = new System.Drawing.Point(128, 153);
            this.tbphone.Name = "tbphone";
            this.tbphone.Size = new System.Drawing.Size(128, 20);
            this.tbphone.TabIndex = 28;
            // 
            // tblast_name
            // 
            this.tblast_name.Location = new System.Drawing.Point(128, 100);
            this.tblast_name.Name = "tblast_name";
            this.tblast_name.Size = new System.Drawing.Size(128, 20);
            this.tblast_name.TabIndex = 27;
            // 
            // tbfirst_name
            // 
            this.tbfirst_name.Location = new System.Drawing.Point(128, 74);
            this.tbfirst_name.Name = "tbfirst_name";
            this.tbfirst_name.Size = new System.Drawing.Size(128, 20);
            this.tbfirst_name.TabIndex = 26;
            // 
            // lbllast_name
            // 
            this.lbllast_name.AutoSize = true;
            this.lbllast_name.Location = new System.Drawing.Point(61, 103);
            this.lbllast_name.Name = "lbllast_name";
            this.lbllast_name.Size = new System.Drawing.Size(56, 13);
            this.lbllast_name.TabIndex = 25;
            this.lbllast_name.Text = "Прізвище";
            this.lbllast_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblphone
            // 
            this.lblphone.AutoSize = true;
            this.lblphone.Location = new System.Drawing.Point(23, 160);
            this.lblphone.Name = "lblphone";
            this.lblphone.Size = new System.Drawing.Size(94, 13);
            this.lblphone.TabIndex = 24;
            this.lblphone.Text = "Номер Телефону";
            this.lblphone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbljob_title_id
            // 
            this.lbljob_title_id.AutoSize = true;
            this.lbljob_title_id.Location = new System.Drawing.Point(43, 128);
            this.lbljob_title_id.Name = "lbljob_title_id";
            this.lbljob_title_id.Size = new System.Drawing.Size(74, 13);
            this.lbljob_title_id.TabIndex = 23;
            this.lbljob_title_id.Text = "Спеціалізація";
            this.lbljob_title_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblmail
            // 
            this.lblmail.AutoSize = true;
            this.lblmail.Location = new System.Drawing.Point(77, 182);
            this.lblmail.Name = "lblmail";
            this.lblmail.Size = new System.Drawing.Size(40, 13);
            this.lblmail.TabIndex = 22;
            this.lblmail.Text = "Пошта";
            this.lblmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbladdress
            // 
            this.lbladdress.AutoSize = true;
            this.lbladdress.Location = new System.Drawing.Point(73, 208);
            this.lbladdress.Name = "lbladdress";
            this.lbladdress.Size = new System.Drawing.Size(44, 13);
            this.lbladdress.TabIndex = 21;
            this.lbladdress.Text = "Адреса";
            this.lbladdress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblfirst_name
            // 
            this.lblfirst_name.AutoSize = true;
            this.lblfirst_name.Location = new System.Drawing.Point(91, 77);
            this.lblfirst_name.Name = "lblfirst_name";
            this.lblfirst_name.Size = new System.Drawing.Size(26, 13);
            this.lblfirst_name.TabIndex = 20;
            this.lblfirst_name.Text = "Ім\'я";
            this.lblfirst_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(128, 283);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(128, 30);
            this.btnSave.TabIndex = 36;
            this.btnSave.Text = "Зберегти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "ID Працівника";
            // 
            // cmbWorkersID
            // 
            this.cmbWorkersID.FormattingEnabled = true;
            this.cmbWorkersID.Location = new System.Drawing.Point(128, 47);
            this.cmbWorkersID.Name = "cmbWorkersID";
            this.cmbWorkersID.Size = new System.Drawing.Size(128, 21);
            this.cmbWorkersID.TabIndex = 39;
            this.cmbWorkersID.SelectedIndexChanged += new System.EventHandler(this.cmbWorkersID_SelectedIndexChanged);
            // 
            // WindowUpdateWorkers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 390);
            this.Controls.Add(this.cmbWorkersID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbJobTitle);
            this.Controls.Add(this.tbbirthday);
            this.Controls.Add(this.tbstart_job);
            this.Controls.Add(this.lblbirthday);
            this.Controls.Add(this.lblstart_job);
            this.Controls.Add(this.tbaddress);
            this.Controls.Add(this.tbmail);
            this.Controls.Add(this.tbphone);
            this.Controls.Add(this.tblast_name);
            this.Controls.Add(this.tbfirst_name);
            this.Controls.Add(this.lbllast_name);
            this.Controls.Add(this.lblphone);
            this.Controls.Add(this.lbljob_title_id);
            this.Controls.Add(this.lblmail);
            this.Controls.Add(this.lbladdress);
            this.Controls.Add(this.lblfirst_name);
            this.Name = "WindowUpdateWorkers";
            this.Text = "WindowUpdateWorkers";
            this.Load += new System.EventHandler(this.WindowUpdateWorkers_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbJobTitle;
        private System.Windows.Forms.TextBox tbbirthday;
        private System.Windows.Forms.TextBox tbstart_job;
        private System.Windows.Forms.Label lblbirthday;
        private System.Windows.Forms.Label lblstart_job;
        private System.Windows.Forms.TextBox tbaddress;
        private System.Windows.Forms.TextBox tbmail;
        private System.Windows.Forms.TextBox tbphone;
        private System.Windows.Forms.TextBox tblast_name;
        private System.Windows.Forms.TextBox tbfirst_name;
        private System.Windows.Forms.Label lbllast_name;
        private System.Windows.Forms.Label lblphone;
        private System.Windows.Forms.Label lbljob_title_id;
        private System.Windows.Forms.Label lblmail;
        private System.Windows.Forms.Label lbladdress;
        private System.Windows.Forms.Label lblfirst_name;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbWorkersID;
    }
}
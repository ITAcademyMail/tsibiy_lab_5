﻿namespace Tsibiy_Lab_5
{
    partial class InsertWorkers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblfirst_name = new System.Windows.Forms.Label();
            this.lbladdress = new System.Windows.Forms.Label();
            this.lblmail = new System.Windows.Forms.Label();
            this.lbljob_title_id = new System.Windows.Forms.Label();
            this.lblphone = new System.Windows.Forms.Label();
            this.lbllast_name = new System.Windows.Forms.Label();
            this.tbfirst_name = new System.Windows.Forms.TextBox();
            this.tblast_name = new System.Windows.Forms.TextBox();
            this.tbphone = new System.Windows.Forms.TextBox();
            this.tbmail = new System.Windows.Forms.TextBox();
            this.tbaddress = new System.Windows.Forms.TextBox();
            this.lblstart_job = new System.Windows.Forms.Label();
            this.lblbirthday = new System.Windows.Forms.Label();
            this.tbstart_job = new System.Windows.Forms.TextBox();
            this.tbbirthday = new System.Windows.Forms.TextBox();
            this.cmbJobTitle = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblfirst_name
            // 
            this.lblfirst_name.AutoSize = true;
            this.lblfirst_name.Location = new System.Drawing.Point(85, 46);
            this.lblfirst_name.Name = "lblfirst_name";
            this.lblfirst_name.Size = new System.Drawing.Size(26, 13);
            this.lblfirst_name.TabIndex = 1;
            this.lblfirst_name.Text = "Ім\'я";
            this.lblfirst_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblfirst_name.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbladdress
            // 
            this.lbladdress.AutoSize = true;
            this.lbladdress.Location = new System.Drawing.Point(67, 177);
            this.lbladdress.Name = "lbladdress";
            this.lbladdress.Size = new System.Drawing.Size(44, 13);
            this.lbladdress.TabIndex = 2;
            this.lbladdress.Text = "Адреса";
            this.lbladdress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbladdress.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblmail
            // 
            this.lblmail.AutoSize = true;
            this.lblmail.Location = new System.Drawing.Point(71, 151);
            this.lblmail.Name = "lblmail";
            this.lblmail.Size = new System.Drawing.Size(40, 13);
            this.lblmail.TabIndex = 3;
            this.lblmail.Text = "Пошта";
            this.lblmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbljob_title_id
            // 
            this.lbljob_title_id.AutoSize = true;
            this.lbljob_title_id.Location = new System.Drawing.Point(37, 97);
            this.lbljob_title_id.Name = "lbljob_title_id";
            this.lbljob_title_id.Size = new System.Drawing.Size(74, 13);
            this.lbljob_title_id.TabIndex = 4;
            this.lbljob_title_id.Text = "Спеціалізація";
            this.lbljob_title_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbljob_title_id.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblphone
            // 
            this.lblphone.AutoSize = true;
            this.lblphone.Location = new System.Drawing.Point(17, 129);
            this.lblphone.Name = "lblphone";
            this.lblphone.Size = new System.Drawing.Size(94, 13);
            this.lblphone.TabIndex = 5;
            this.lblphone.Text = "Номер Телефону";
            this.lblphone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblphone.Click += new System.EventHandler(this.label5_Click);
            // 
            // lbllast_name
            // 
            this.lbllast_name.AutoSize = true;
            this.lbllast_name.Location = new System.Drawing.Point(55, 72);
            this.lbllast_name.Name = "lbllast_name";
            this.lbllast_name.Size = new System.Drawing.Size(56, 13);
            this.lbllast_name.TabIndex = 6;
            this.lbllast_name.Text = "Прізвище";
            this.lbllast_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbllast_name.Click += new System.EventHandler(this.label6_Click);
            // 
            // tbfirst_name
            // 
            this.tbfirst_name.Location = new System.Drawing.Point(122, 43);
            this.tbfirst_name.Name = "tbfirst_name";
            this.tbfirst_name.Size = new System.Drawing.Size(128, 20);
            this.tbfirst_name.TabIndex = 7;
            this.tbfirst_name.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tblast_name
            // 
            this.tblast_name.Location = new System.Drawing.Point(122, 69);
            this.tblast_name.Name = "tblast_name";
            this.tblast_name.Size = new System.Drawing.Size(128, 20);
            this.tblast_name.TabIndex = 8;
            // 
            // tbphone
            // 
            this.tbphone.Location = new System.Drawing.Point(122, 122);
            this.tbphone.Name = "tbphone";
            this.tbphone.Size = new System.Drawing.Size(128, 20);
            this.tbphone.TabIndex = 10;
            // 
            // tbmail
            // 
            this.tbmail.Location = new System.Drawing.Point(122, 148);
            this.tbmail.Name = "tbmail";
            this.tbmail.Size = new System.Drawing.Size(128, 20);
            this.tbmail.TabIndex = 11;
            this.tbmail.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // tbaddress
            // 
            this.tbaddress.Location = new System.Drawing.Point(122, 174);
            this.tbaddress.Name = "tbaddress";
            this.tbaddress.Size = new System.Drawing.Size(128, 20);
            this.tbaddress.TabIndex = 12;
            // 
            // lblstart_job
            // 
            this.lblstart_job.AutoSize = true;
            this.lblstart_job.Location = new System.Drawing.Point(3, 229);
            this.lblstart_job.Name = "lblstart_job";
            this.lblstart_job.Size = new System.Drawing.Size(113, 13);
            this.lblstart_job.TabIndex = 13;
            this.lblstart_job.Text = "Розпочав працювати";
            this.lblstart_job.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblstart_job.Click += new System.EventHandler(this.label6_Click_1);
            // 
            // lblbirthday
            // 
            this.lblbirthday.AutoSize = true;
            this.lblbirthday.Location = new System.Drawing.Point(13, 203);
            this.lblbirthday.Name = "lblbirthday";
            this.lblbirthday.Size = new System.Drawing.Size(98, 13);
            this.lblbirthday.TabIndex = 14;
            this.lblbirthday.Text = "Дата народження";
            this.lblbirthday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblbirthday.Click += new System.EventHandler(this.label7_Click);
            // 
            // tbstart_job
            // 
            this.tbstart_job.Location = new System.Drawing.Point(122, 226);
            this.tbstart_job.Name = "tbstart_job";
            this.tbstart_job.Size = new System.Drawing.Size(128, 20);
            this.tbstart_job.TabIndex = 15;
            // 
            // tbbirthday
            // 
            this.tbbirthday.Location = new System.Drawing.Point(122, 200);
            this.tbbirthday.Name = "tbbirthday";
            this.tbbirthday.Size = new System.Drawing.Size(128, 20);
            this.tbbirthday.TabIndex = 16;
            this.tbbirthday.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // cmbJobTitle
            // 
            this.cmbJobTitle.FormattingEnabled = true;
            this.cmbJobTitle.Location = new System.Drawing.Point(122, 94);
            this.cmbJobTitle.Name = "cmbJobTitle";
            this.cmbJobTitle.Size = new System.Drawing.Size(128, 21);
            this.cmbJobTitle.TabIndex = 17;
            this.cmbJobTitle.SelectedIndexChanged += new System.EventHandler(this.cmbJobTitle_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(122, 252);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(128, 30);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Зберегти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 25);
            this.label1.TabIndex = 19;
            this.label1.Text = "Таблиця Працівників";
            // 
            // InsertWorkers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 301);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbJobTitle);
            this.Controls.Add(this.tbbirthday);
            this.Controls.Add(this.tbstart_job);
            this.Controls.Add(this.lblbirthday);
            this.Controls.Add(this.lblstart_job);
            this.Controls.Add(this.tbaddress);
            this.Controls.Add(this.tbmail);
            this.Controls.Add(this.tbphone);
            this.Controls.Add(this.tblast_name);
            this.Controls.Add(this.tbfirst_name);
            this.Controls.Add(this.lbllast_name);
            this.Controls.Add(this.lblphone);
            this.Controls.Add(this.lbljob_title_id);
            this.Controls.Add(this.lblmail);
            this.Controls.Add(this.lbladdress);
            this.Controls.Add(this.lblfirst_name);
            this.Name = "InsertWorkers";
            this.Text = "InsertWorkers";
            this.Load += new System.EventHandler(this.InsertWorkers_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblfirst_name;
        private System.Windows.Forms.Label lbladdress;
        private System.Windows.Forms.Label lblmail;
        private System.Windows.Forms.Label lbljob_title_id;
        private System.Windows.Forms.Label lblphone;
        private System.Windows.Forms.Label lbllast_name;
        private System.Windows.Forms.TextBox tbfirst_name;
        private System.Windows.Forms.TextBox tblast_name;
        private System.Windows.Forms.TextBox tbphone;
        private System.Windows.Forms.TextBox tbmail;
        private System.Windows.Forms.TextBox tbaddress;
        private System.Windows.Forms.Label lblstart_job;
        private System.Windows.Forms.Label lblbirthday;
        private System.Windows.Forms.TextBox tbstart_job;
        private System.Windows.Forms.TextBox tbbirthday;
        private System.Windows.Forms.ComboBox cmbJobTitle;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
    }
}
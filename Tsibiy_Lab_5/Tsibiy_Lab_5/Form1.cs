﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Tsibiy_Lab_5
{
    public partial class Form1 : Form
    {
        string ConnectionString = "Server=tcp:itacademyukraine.database.windows.net;Database=r.cibiy;User ID =r.cibiy;Password=Dbiyz117;Trusted_Connection=False;Encrypt=True;";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadFlightTeam();
            LoadGOlanding();
            LoadDistance();
        }
        
        private void LoadFlightTeam()
        {
            
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlCommand comm = new SqlCommand("SELEct * from Flight", con);
            SqlDataReader DR = comm.ExecuteReader();
            cmbFlight.Items.Clear();

            while (DR.Read())
            {
                Flight fl = new Flight();
                fl.id = (int)DR[0];
                cmbFlight.Items.Add(fl);
                cmbFlight.DisplayMember = "id";
                
            }
            con.Close();
        }

        private void btnFlightTeam_Click(object sender, EventArgs e)
        {
            try
            {
                    
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            Flight FL = (Flight)cmbFlight.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter("SELECT F.departure_date[Дата відправлення], F.flight_id[Номер польоту], A.[serias_number_airplane][Тип літака], A.[number_passengers][Пасажиромісткість], W.first_name[Ім'я], W.last_name[Прізвище], W.start_job[Дата прийому на роботу], W.birthday[Дата народження] FROM (((Airplane AS A INNER JOIN Flight AS F ON A.airplane_id = F.airplane_id) INNER JOIN Flight_team AS FT ON F.flight_id = FT.flight_id) INNER JOIN Workers AS W ON W.workers_id = FT.workers_id) WHERE  F.flight_id = " + FL.id.ToString(), con);
            DataSet DS = new DataSet();
            DA.Fill(DS, "ResultsFlightTeam");
            dgvResult.DataSource = DS.Tables["ResultsFlightTeam"];
            dgvResult.Refresh();           
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message.ToString());
            }
        }

        private void LoadGOlanding()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlCommand comm = new SqlCommand("select distinct [landing_сity] from Flight ", con);
            SqlDataReader DR = comm.ExecuteReader();
            cmbGOlanding.Items.Clear();
            while (DR.Read())
            {
                Flight fl = new Flight();
                fl.landing_сity = (string)DR[0];
                cmbGOlanding.Items.Add(fl);
                cmbGOlanding.DisplayMember = "landing_сity";

            }
            con.Close();
        }

        private void btnGoKiev_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString);
                con.Open();
                Flight FL = (Flight)cmbGOlanding.SelectedItem;
                SqlDataAdapter DA = new SqlDataAdapter("SELECT P.first_name[Ім'я],P.last_name[Прізвище],F.flight_id[Номер польоту], F.[departure_сity][Місто відправлення], F.[landing_сity][Місто призначення],T.number_ticket[Номер квитка] FROM ((Flight AS F  INNER JOIN Tickets AS T ON F.flight_id = T.flight_id) INNER JOIN Passengers AS P ON P.passenger_id = T.passenger_id) WHERE F.[landing_сity] LIKE '" + FL.landing_сity.ToString() + "' ", con);
                DataSet DS = new DataSet();
                DA.Fill(DS, "ResultsGOlanding");
                dgvResult.DataSource = DS.Tables["ResultsGOlanding"];
                dgvResult.Refresh();
                con.Close();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message.ToString());

            }
        }

        private void LoadDistance()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlCommand comm = new SqlCommand("select distinct distance  from airplane ", con);
            SqlDataReader DR = comm.ExecuteReader();
            cmbdistance.Items.Clear();
            while (DR.Read())
            {
                Airplane ai = new Airplane();
                ai.distance = (int)DR[0];
                cmbdistance.Items.Add(ai);
                cmbdistance.DisplayMember = "distance";
            }
            con.Close();
        }

        private void bntAirplaneDistanceMor500_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString);
                con.Open();
                Airplane AI = (Airplane)cmbdistance.SelectedItem;
                SqlDataAdapter DA = new SqlDataAdapter(" SELECT [flight_id][Номер рейсу], [departure_сity][Місто відправки],[transit_city][Транзитне місто], [landing_сity][Місто прибуття], distance[Дальність польоту] FROM     Airplane AS A INNER JOIN Flight AS F ON A.airplane_id = F.airplane_id WHERE  A.distance >= " + AI.distance.ToString(), con);
                DataSet DS = new DataSet();
                DA.Fill(DS, "ResultsAirplaneDistance");
                dgvResult.DataSource = DS.Tables["ResultsAirplaneDistance"];
                dgvResult.Refresh();
                con.Close();

            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message.ToString());
            }
        }


        private void btnWorkers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlDataAdapter DA = new SqlDataAdapter("select * from workers", con);
            DataSet DS = new DataSet();
            DA.Fill(DS,"ResultsWorkersClick");
            dgvResult.DataSource = DS.Tables["ResultsWorkersClick"];
            dgvResult.Refresh();
            con.Close();
            
        }


        private void btnUpdateWorkers_Click(object sender, EventArgs e)
        {
            WindowUpdateWorkers WUW = new WindowUpdateWorkers();
            WUW.Show();
        }











        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void btnInsertDate_Click(object sender, EventArgs e)
        {
            InsertWorkers IW = new InsertWorkers();
            IW.Show();
        }








        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbPassengers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbmFlight_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbdistance_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}

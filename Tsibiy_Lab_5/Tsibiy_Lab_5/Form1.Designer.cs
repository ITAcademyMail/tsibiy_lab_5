﻿namespace Tsibiy_Lab_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmbdistance = new System.Windows.Forms.ComboBox();
            this.cmbGOlanding = new System.Windows.Forms.ComboBox();
            this.cmbFlight = new System.Windows.Forms.ComboBox();
            this.lblWorkers = new System.Windows.Forms.Label();
            this.btnInsertWorkers = new System.Windows.Forms.Button();
            this.bntAirplaneDistance = new System.Windows.Forms.Button();
            this.btnGOlanding = new System.Windows.Forms.Button();
            this.btnFlightTeam = new System.Windows.Forms.Button();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.btnWorkers = new System.Windows.Forms.Button();
            this.btnUpdateWorkers = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdateWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.btnWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.cmbdistance);
            this.splitContainer1.Panel1.Controls.Add(this.cmbGOlanding);
            this.splitContainer1.Panel1.Controls.Add(this.cmbFlight);
            this.splitContainer1.Panel1.Controls.Add(this.lblWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.btnInsertWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.bntAirplaneDistance);
            this.splitContainer1.Panel1.Controls.Add(this.btnGOlanding);
            this.splitContainer1.Panel1.Controls.Add(this.btnFlightTeam);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvResult);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(879, 450);
            this.splitContainer1.SplitterDistance = 130;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // cmbdistance
            // 
            this.cmbdistance.FormattingEnabled = true;
            this.cmbdistance.Location = new System.Drawing.Point(746, 71);
            this.cmbdistance.Name = "cmbdistance";
            this.cmbdistance.Size = new System.Drawing.Size(121, 21);
            this.cmbdistance.TabIndex = 9;
            this.cmbdistance.SelectedIndexChanged += new System.EventHandler(this.cmbdistance_SelectedIndexChanged);
            // 
            // cmbGOlanding
            // 
            this.cmbGOlanding.FormattingEnabled = true;
            this.cmbGOlanding.Location = new System.Drawing.Point(746, 41);
            this.cmbGOlanding.Name = "cmbGOlanding";
            this.cmbGOlanding.Size = new System.Drawing.Size(121, 21);
            this.cmbGOlanding.TabIndex = 8;
            // 
            // cmbFlight
            // 
            this.cmbFlight.FormattingEnabled = true;
            this.cmbFlight.Location = new System.Drawing.Point(746, 12);
            this.cmbFlight.Name = "cmbFlight";
            this.cmbFlight.Size = new System.Drawing.Size(121, 21);
            this.cmbFlight.TabIndex = 7;
            this.cmbFlight.SelectedIndexChanged += new System.EventHandler(this.cbmFlight_SelectedIndexChanged);
            // 
            // lblWorkers
            // 
            this.lblWorkers.AutoSize = true;
            this.lblWorkers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWorkers.Location = new System.Drawing.Point(12, 15);
            this.lblWorkers.Name = "lblWorkers";
            this.lblWorkers.Size = new System.Drawing.Size(84, 16);
            this.lblWorkers.TabIndex = 6;
            this.lblWorkers.Text = "Працівники";
            // 
            // btnInsertWorkers
            // 
            this.btnInsertWorkers.Location = new System.Drawing.Point(12, 68);
            this.btnInsertWorkers.Name = "btnInsertWorkers";
            this.btnInsertWorkers.Size = new System.Drawing.Size(144, 24);
            this.btnInsertWorkers.TabIndex = 5;
            this.btnInsertWorkers.Text = "Внести";
            this.btnInsertWorkers.UseVisualStyleBackColor = true;
            this.btnInsertWorkers.Click += new System.EventHandler(this.btnInsertDate_Click);
            // 
            // bntAirplaneDistance
            // 
            this.bntAirplaneDistance.Location = new System.Drawing.Point(665, 68);
            this.bntAirplaneDistance.Name = "bntAirplaneDistance";
            this.bntAirplaneDistance.Size = new System.Drawing.Size(75, 24);
            this.bntAirplaneDistance.TabIndex = 4;
            this.bntAirplaneDistance.Text = "Запит 3";
            this.bntAirplaneDistance.UseVisualStyleBackColor = true;
            this.bntAirplaneDistance.Click += new System.EventHandler(this.bntAirplaneDistanceMor500_Click);
            // 
            // btnGOlanding
            // 
            this.btnGOlanding.Location = new System.Drawing.Point(665, 41);
            this.btnGOlanding.Name = "btnGOlanding";
            this.btnGOlanding.Size = new System.Drawing.Size(75, 24);
            this.btnGOlanding.TabIndex = 3;
            this.btnGOlanding.Text = "Запит 2";
            this.btnGOlanding.UseVisualStyleBackColor = true;
            this.btnGOlanding.Click += new System.EventHandler(this.btnGoKiev_Click);
            // 
            // btnFlightTeam
            // 
            this.btnFlightTeam.Location = new System.Drawing.Point(665, 12);
            this.btnFlightTeam.Name = "btnFlightTeam";
            this.btnFlightTeam.Size = new System.Drawing.Size(75, 23);
            this.btnFlightTeam.TabIndex = 2;
            this.btnFlightTeam.Text = "Запит 1";
            this.btnFlightTeam.UseVisualStyleBackColor = true;
            this.btnFlightTeam.Click += new System.EventHandler(this.btnFlightTeam_Click);
            // 
            // dgvResult
            // 
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvResult.Location = new System.Drawing.Point(0, 0);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.Size = new System.Drawing.Size(879, 316);
            this.dgvResult.TabIndex = 0;
            this.dgvResult.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResult_CellContentClick);
            // 
            // btnWorkers
            // 
            this.btnWorkers.Location = new System.Drawing.Point(12, 39);
            this.btnWorkers.Name = "btnWorkers";
            this.btnWorkers.Size = new System.Drawing.Size(144, 23);
            this.btnWorkers.TabIndex = 10;
            this.btnWorkers.Text = "Переглянути";
            this.btnWorkers.UseVisualStyleBackColor = true;
            this.btnWorkers.Click += new System.EventHandler(this.btnWorkers_Click);
            // 
            // btnUpdateWorkers
            // 
            this.btnUpdateWorkers.Location = new System.Drawing.Point(12, 98);
            this.btnUpdateWorkers.Name = "btnUpdateWorkers";
            this.btnUpdateWorkers.Size = new System.Drawing.Size(144, 23);
            this.btnUpdateWorkers.TabIndex = 11;
            this.btnUpdateWorkers.Text = "Оновити";
            this.btnUpdateWorkers.UseVisualStyleBackColor = true;
            this.btnUpdateWorkers.Click += new System.EventHandler(this.btnUpdateWorkers_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.Button btnFlightTeam;
        private System.Windows.Forms.Button btnGOlanding;
        private System.Windows.Forms.Button bntAirplaneDistance;
        private System.Windows.Forms.Button btnInsertWorkers;
        private System.Windows.Forms.Label lblWorkers;
        private System.Windows.Forms.ComboBox cmbFlight;
        private System.Windows.Forms.ComboBox cmbGOlanding;
        private System.Windows.Forms.ComboBox cmbdistance;
        private System.Windows.Forms.Button btnWorkers;
        private System.Windows.Forms.Button btnUpdateWorkers;
    }
}

